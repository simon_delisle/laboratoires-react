import React from 'react';
import {Link} from 'react-router-dom';


class Genliste extends React.Component{
    render(){
        return(
            <tr>
                <td>{this.props.utilisateurs.code}</td>
                <td>{this.props.utilisateurs.nom}</td>
                <td>{this.props.utilisateurs.prenom}</td>
                <td>{this.props.utilisateurs.dep}</td>
                <td>||<Link to={"/edit/" + this.props.utilisateurs._id}>Edit</Link>||
                    <a href="#" onClick={()=>
                    {this.props.delstd(this.props.utilisateurs._id)}
                    }>Delete</a>||</td>  {/* eventlistener click pour deleter a partir du id */}
            </tr>
           
        )
    }
}
export default Genliste;
