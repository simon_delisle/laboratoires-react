import React from 'react';
import Genlist from './genliste';
import axios from 'axios';

/* 
let utils = [{code:'GreMa', nom:'Grenier', prenom:'Marc'},
                {code:'RoyPa', nom:'Roy', prenom:'Patrick'},
            ]; */


class Liste extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            utilisateurs:[]
        }        
    }
    

    componentDidMount(){
        this.delstd = this.delstd.bind(this);
       /*  this.setState({utilisateurs:utils}) */
    
       axios.get('http://10.30.40.121:3877/etudiant')

       .then(response => {                                                              //attendre le retour du chargement de la reponse
        console.log(response.data);
            if(response.data.count > 0)                                                //test si il y a bien un data avant tout
            {                
                this.setState({
                    
                    utilisateurs: response.data.data                                     //passe linfo de la bd venant du back end node au tableau utilisateurs[]
                })
            }
       })
       .catch((error)=>{
           console.log(error);
       })
    }


    delstd(id){
        axios.delete('http://10.30.40.121:3877/'+id).then(res=> console.log(res.data));
        this.setState({
            utilisateurs: this.state.utilisateurs.filter(el =>el._id !==id) //garde tout les elements qui ne sont pas le Id selectionner 
                                                                                //et delete ce id selectionner
        })
    }



    userList(){
        return this.state.utilisateurs.map(utilCourant =>{
            return <Genlist utilisateurs = {utilCourant} delstd={this.delstd} key = {utilCourant._id}/>;
        });
    }
    render(){
        return(
            <div className="container">
                
                <h3>Liste des utilisateurs</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Code</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>Departement</th>
                        </tr>
                    </thead>
                                <tbody>                                   
                                     {this.userList()}  {/* charge la function userlist de la class Liste! */}
                                </tbody>
                </table>
            </div>
           
        )
    }
}
export default Liste;

